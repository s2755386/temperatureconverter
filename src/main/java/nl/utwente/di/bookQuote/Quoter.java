package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    public double getBookPrice(String isbn) {
        //°F = (°C × 9/5) + 32
        return (Double.parseDouble(isbn) * 9 / 5) + 32;
    }
}
